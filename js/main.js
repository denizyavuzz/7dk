// const navbar = document.getElementById("navbar");
// let scrolled = false;

// window.onscroll = function () {
//   if (window.pageYOffset > window.innerHeight) {
//     navbar.classList.remove("top");
//     if (!scrolled) {
//       navbar.style.transform = "translateY(-70px)";
//     }
//     this.setTimeout(function () {
//       navbar.style.transform = "translateY(0px)";
//       scrolled = true;
//     }, 500);
//   } else {
//     navbar.classList.add("top");
//     scrolled = false;
//   }
// };

// hamburger menu

let mainNav = document.getElementById("js-menu");
let backGround = document.getElementById("navbar");
let navBarToggle = document.getElementById("js-navbar-toggle");

navBarToggle.addEventListener("click", function () {
  mainNav.classList.toggle("active");
});

navBarToggle.addEventListener("click", function () {
  backGround.classList.toggle("navBack");
});

// type effect beginning

const TypeWriter = function (txtElement, words, wait = 1000) {
  this.txtElement = txtElement;
  this.words = words;
  this.txt = "";
  this.wordIndex = 0;
  this.wait = wait;
  this.type();
  this.isDeleting = false;
};

// type method

TypeWriter.prototype.type = function () {
  // current index of word
  const current = this.wordIndex % this.words.length;

  // get full text of current word

  const fullTxt = this.words[current];

  // check if deleting

  if (this.isDeleting) {
    // remove char
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    // add char
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  // insert txt into element
  this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

  // initial type speed
  let typeSpeed = 100 + 100 * Math.random();

  if (this.isDeleting) {
    typeSpeed = 100;
  }

  // if word is complete

  if (!this.isDeleting && this.txt === fullTxt) {
    // make pause at end
    typeSpeed = this.wait;

    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    // move to next word
    this.wordIndex++;
    // pause before retyping
    typeSpeed = 500;
  }

  setTimeout(() => this.type(), typeSpeed);
};

// init on DOM load

document.addEventListener("DOMContentLoaded", init);

// init app

function init() {
  const txtElement = document.querySelector(".txt-type");
  const words = JSON.parse(txtElement.getAttribute("data-words"));
  const wait = txtElement.getAttribute("data-wait");

  // init typewriter

  new TypeWriter(txtElement, words, wait);
}
